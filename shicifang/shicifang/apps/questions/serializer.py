from rest_framework import serializers
from rest_framework.response import Response

from articles.models import Article
from comments.models import Spit
from questions.models import Label, Question, Reply
from users.models import User


# 李钰譞
class QuestionSerializer_lxy(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    labels = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ('id', 'createtime', 'labels',
                  'reply', 'replyname', 'replytime',
                  'title', 'unuseful_count', 'useful_count',
                  'user', 'visits')


# 李钰譞
class ArticlesSerializer_lxy2(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'title')


# 李钰譞
class UserSerializer_lxy(serializers.ModelSerializer):
    fans = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    articles = ArticlesSerializer_lxy2(many=True, read_only=True)

    class Meta:
        model = User
        fields = ('id', 'articles', 'avatar',
                  'fans', 'username')


# 李钰譞
class SpitSerializer_lxy(serializers.ModelSerializer):
    class Meta:
        model = Spit
        fields = ('collected')


# 李钰譞
class ArticlesSerializer_lxy(serializers.ModelSerializer):
    user = UserSerializer_lxy(read_only=True)
    collected_users = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    collected = SpitSerializer_lxy(read_only=True)

    class Meta:
        model = Article
        fields = ('id', 'collected_users', 'collected',
                  'content', 'createtime', 'image',
                  'title', 'user', 'visits')


# 李钰譞
class LabelSerializer_lxy(serializers.ModelSerializer):
    questions = QuestionSerializer_lxy(many=True, read_only=True)
    articles = ArticlesSerializer_lxy(many=True, read_only=True)

    class Meta:
        model = Label
        fields = ('id', 'articles', 'label_name', 'desc', 'baike_url', 'label_icon', 'users', 'questions',)
