from rest_framework import serializers
from questions.models import Label, Question, Reply
from users.models import User
from users.models import User
from .models import Question, Label, Reply
#揣培洋 --标签序列化器
class LabelSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ["id", "label_name"]

#揣培洋 --问题序列化器
class QuestionSerializerForList(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)
    labels = serializers.StringRelatedField(read_only=True,many=True)

    class Meta:
        model = Question
        fields = ["id","createtime","labels","reply","replyname","replytime","title","unuseful_count","useful_count","user","visits"]


#揣培洋 --评论序列化器
class ReplySerializerForCreate(serializers.ModelSerializer):

    class Meta:
        model = Reply
        fields = '__all__'

#揣培洋 --用户添加序列化器
#class UserSerializerSimple(serializers.ModelSerializer):



class LabelsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Label
        fields = ("label_name",)

class QuestionSerializer(serializers.ModelSerializer):

    labels = LabelsSerializer(read_only=True, many=True)
    user = serializers.StringRelatedField(label='昵称', read_only=True)

    class Meta:
        model = Question
        fields = ('createtime','id','labels','reply',
                  'replyname',
                  'replytime', 'title','unuseful_count',
                  'useful_count','user','visits')

class QaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        field = '__all__'

# class SubSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Reply
#         fields = ("label_name")

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'avatar')

#揣培洋 --评论序列化器
class ReplySerializerForSubAndParent(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = ["id", "content","createtime","useful_count","unuseful_count","user"]

#揣培洋 --评论序列化器
class ReplySerializerForList(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    subs = ReplySerializerForSubAndParent(read_only=True, many=True)
    parent = ReplySerializerForSubAndParent(read_only=True)

    class Meta:
        model = Reply
        fields = ["id", "content","createtime","useful_count",'problem',"unuseful_count","subs","user","parent"]

# class SubsSerializer(serializers.ModelSerializer):


class SubsSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Reply
        fields = ('id', 'content', 'createtime', 'useful_count', 'unuseful_count',
                 'user')

class Comment_questionSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    subs = SubsSerializer (read_only=True, many=True)

    class Meta:
        model = Question
        fields = ('id', 'content', 'createtime', 'useful_count','problem',
                  'unuseful_count', 'subs',
                 'user', 'parent')

class Answer_questionSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    subs = SubsSerializer (read_only=True, many=True)

    class Meta:
        model = Question
        fields = ('id','content', 'createtime', 'userful_count', 'problem',
                'unuseful_count', 'subs', 'user', 'parent', )

class QuestionmessSerializer(serializers.ModelSerializer):
    labels = LabelsSerializer(read_only=True, many = True)
    comment_question = Comment_questionSerializer(read_only=True, many=True)
    answer_question = Answer_questionSerializer(read_only=True, many = True)

    class Meta:
        model = Question
        fields = ('id','createtime','labels','reply',
                  'replyname',
                  'replytime', 'title','unuseful_count',
                  'useful_count','user','visits','content','comment_question',
                  'answer_question')

