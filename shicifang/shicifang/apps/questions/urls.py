
from rest_framework.routers import SimpleRouter
from questions import views
# 揣培洋 --4.1 所有标签信息;用户关注标签 GET/labels/
urlpatterns = [
]
router = SimpleRouter()
router.register(r'labels', views.GetViewSet)
urlpatterns += router.urls

from django.urls import re_path, path
from questions import views



urlpatterns = [
    re_path(r'^questions/(?P<id>([1-9]\d*(\.\d+)?|0\.\d*[1-9]))/$', views.Questions.as_view(
        {'get': 'get'}
    )),
    #范璇--4.3最新回答的问题
    re_path(r'^questions/(?P<id>-([1-9]\d*(\.\d+)?|0\.\d*[1-9]))/label/new/$', views.QuestionsAnswer.as_view(
        {'get': 'new'}
    )),
    #范璇--4.4最热回答的问题
    re_path(r'^questions/(?P<id>-([1-9]\d*(\.\d+)?|0\.\d*[1-9]))/label/hot/$', views.QuestionsAnswer.as_view(
        {'get':'hot'}
    )),
    #范璇--4.5等待回答的问题
    re_path(r'^questions/(?P<id>-([1-9]\d*(\.\d+)?|0\.\d*[1-9]))/label/wait/$', views.QuestionsAnswer.as_view(
        {'get':'wait'}
    )),
    #范璇--4.8问题有用
    re_path(r'^questions/(?P<id>-([1-9]\d*(\.\d+)?|0\.\d*[1-9]))/useful/$', views.QuestionsAnswer.as_view(
        {'put':'useful'}
    )),
    #范璇--4.9问题没用
    re_path(r'^questions/(?P<id>-([1-9]\d*(\.\d+)?|0\.\d*[1-9]))/unuseful/$', views.QuestionsAnswer.as_view(
        {'put':'unuseful'}
    )),
    #范璇--4.7问题详情
    re_path(r'^questions/$', views.QuestionsAnswer.as_view(
        {'post':'create'}
    )),

    re_path(r'^reply/$', views.ReplyViewLxy.as_view({
        'post': 'reply'
    })),
    re_path(r'^reply/(?P<pk>\d+)/useful/$', views.ReplyViewLxy.as_view({
        'put': 'useful'
    })),
    re_path(r'^reply/(?P<pk>\d+)/unuseful/$', views.ReplyViewLxy.as_view({
        'put': 'unuseful'
    })),
    re_path(r'^labels/(?P<pk>\d+)/focusin/$', views.ReplyViewLxy.as_view({
        'put': 'LabelFocusin'
    })),
    re_path(r'^labels/(?P<pk>\d+)/focusout/$', views.ReplyViewLxy.as_view({
        'put': 'LabelFocusout'
    })),
    re_path(r'^labels/(?P<pk>\d+)/$', views.LabelViewLxy.as_view()),

]
