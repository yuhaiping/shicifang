from django.urls import re_path
from . import views

urlpatterns = [
    #揣培洋 -- 8.1 获取用户详情
    re_path(r'^user/$', views.UserInFoView.as_view()),
    #揣培洋 -- 8.3 修改密码
    re_path(r'^user/password/$', views.SetPwdView.as_view()),
    # 揣培洋  -- 8.4关注和取消关注
    re_path(r'^users/like/(?P<id>[^/.]+)/$', views.UserLikeView.as_view()),
    #揣培洋 -- 8.2修改个人信息
    re_path(r'^user/label/$', views.UserLabelView.as_view()),

    # re_path(r'^users/$', views.UserRegisterView.as_view()),
    # re_path(r'^authorizations/$', views.UserLoginView.as_view()),
    # re_path(r'^sms_codes/(?P<mobile>.*?)/$', views.SendSmscodeView.as_view()),
    re_path(r"^users/$", views.LoginAuthView.as_view()),
    re_path(r"^authorizations/$", views.UserLogin.as_view()),
    re_path(r"^sms_codes/(?P<mobile>\d{11})/$", views.Sms_code.as_view()),
]





