from django.utils.datetime_safe import datetime
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from activities.models import Gathering
from activities.serializers import GatherSerializerSimple, GatherSerializer


# 王喆--5.1 活动列表  GET /gatherings/
class GathersView(ListAPIView):
    serializer_class = GatherSerializerSimple
    queryset = Gathering.objects.filter(state=1)

# 王喆--5.2 活动详情 GET /gatherings/{id}/
class GatherView(RetrieveAPIView):
    serializer_class = GatherSerializer
    queryset = Gathering.objects.filter(state=1)

# 王喆--5.3 报名活动  GET /gatherings/{id}/join/
class GatherJoinView(GenericAPIView):
    queryset = Gathering.objects.filter(state=1)
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        user = request.user
        gathering = self.get_object()
        now = datetime.now()
        endtime = gathering.endrolltime.replace(tzinfo=None)
        if endtime < now:
            return Response({'success': False, 'message': '报名时间已过'}, status=400)
        else:
            if user in gathering.users.all():
                gathering.users.remove(user)
                gathering.save()
                return Response({'success': True, 'message': '取消成功'})
            else:
                gathering.users.add(user)
                gathering.save()
                return Response({'success':True,'message':'参加成功'})
