from rest_framework import serializers

from activities.models import Gathering
from users.models import User


class GatherSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gathering
        fields = "__all__"

# class UserSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = User
#         fields = ("id")

class GatherSerializerSimple(serializers.ModelSerializer):
    # users = UserSerializer(read_only=True,many=True)
    # users = serializers.PrimaryKeyRelatedField(read_only=True,many=True)
    class Meta:
        model = Gathering
        fields = ("id","name","image","city","starttime","endrolltime","users")
