from django.urls import re_path

import articles
from . import  views
urlpatterns = [

    #文章列表
    # re_path(r'^article/?P<id>\d/channel/$',views.ArticleView.as_view({
    #     "get":"ArticleList"
    # })),
    #
    # #文章详情
    # re_path(r'^article/?P<id>\d/$',views.ArticleView.as_view({
    #     "get":"ArtiDetail"
    # })),
    #
    # #发布文章
    # re_path(r'^article/$',views.ArticleView.as_view({
    #     "post":"CreateArticle"
    # })),
    #
    # #评论文章
    # re_path('^article/?P<id>\d/publish_comment/$',views.ArticleView.as_view({
    #     "post":"CommentArticle"
    # })),
    #
    #关键字搜索
    # re_path(r'^articles/search/$',views.SearchViews.as_view({
    #     "get":"list"
    # })),
    # re_path(r'^articles/(?P<pk>\d+)/$',views.ArticleView.as_view({
    #     "get":"ArticleDetail"
    # })),
    # re_path(r'^articles/$',views.ArticleView.as_view({
    #     "post":"Create"
    # })),
    # re_path(r'^labels/$',views.LabelView.as_view({
    #     "get":"list"
    # })),

    # re_path(r'^article/(?P<pk>\d+)/collect/$',views.ArticleView.as_view({
    #     "put":"collect"
    # })),
    ]

from rest_framework.routers import SimpleRouter

#频道列表url
router = SimpleRouter()
router.register(r'channels', views.ChannelsView, basename='channels')
urlpatterns += router.urls


router = SimpleRouter()
router.register(r'article', views.ArticleView, basename='article')
urlpatterns += router.urls

router = SimpleRouter()
router.register('articles/search', views.SearchViews,basename="search")
urlpatterns += router.urls