from rest_framework import serializers
from comments.models import Spit

# 余海平 -- 6.1 吐槽列表  GET　 /spit/
class SpitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Spit
        fields = ("id", "nickname", "parent", "publishtime",
                  "hasthumbup", "thumbup", "userid", "visits",
                  "content", "avatar", "collected", "comment")
        extra_kwargs = {"nickname": {"read_only": True},
                        "hasthumbup": {"read_only": True},
                        "thumbup": {"read_only": True},
                        "userid": {"read_only": True},
                        "visits": {"read_only": True},
                        "avatar": {"read_only": True},
                        "collected": {"read_only": True},}

    # def create(self, validated_data):
    #     super().create(validated_data)





