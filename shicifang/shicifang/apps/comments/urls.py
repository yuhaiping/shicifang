from django.urls import re_path
from . import views

urlpatterns = [
    # 余海平 -- 6.1 吐槽列表 GET /spit/
    # 余海平 -- 6.4、发布吐槽  POST /spit/
    re_path(r"^spit/$", views.SpitListView.as_view({
        "post": "create",
        "get": "list"
    })),

    # 余海平 -- 6.2 收藏或取消收藏  PUT  /spit/{id}/collect/
    re_path(r"^spit/(?P<pk>\d+)/collect/$", views.SpitViewSet.as_view({
        "put": "collected"
    })),
    # 余海平 -- 6.3、点赞或取消点赞 PUT /spit/{id}/updatethumbup/
    re_path(r"^spit/(?P<pk>\d+)/updatethumbup/$", views.SpitViewSet.as_view({
        "put": "updatethumbup"
    })),
    # 余海平 -- 6.5、吐槽详情  GET  /spit/{id}/
    re_path(r"^spit/(?P<pk>\d+)/$", views.SpitListView.as_view({
        "get": "retrieve"
    })),
    # 余海平 -- 6.6、获取吐槽评论  GET   /spit/{id}/children/
    re_path(r"^spit/(?P<pk>\d+)/children/$", views.SpitListView.as_view({
        "get": "children"
    })),
]