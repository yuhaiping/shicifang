from rest_framework import serializers
from jobs.models import Enterprise


class EnterpriseSerializerSimple(serializers.ModelSerializer):
    # 揣培洋 --8.1 企业序列化器
    class Meta:
        model = Enterprise
        fields = ('id', 'name','labels','logo','recruits','summary')



