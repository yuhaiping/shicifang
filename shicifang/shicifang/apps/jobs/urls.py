from django.urls import re_path

from jobs import views


from rest_framework.routers import SimpleRouter
urlpatterns = [
    re_path(r"^enterprise/search/hotlist/$", views.EnterpriseViewSet.as_view({
        "get": "get_hot_enterprise"
    }))
]


router = SimpleRouter()
router.register(r'enterprise', views.EnterpriseViewSet)


# ① 创建Router对象
from rest_framework.routers import SimpleRouter
router = SimpleRouter()
# ② 注册视图集
router.register(r'recruits', views.RecruitViewSet)
router.register(r'city', views.CityViewSet)
# ③ 添加url配置

urlpatterns += router.urls