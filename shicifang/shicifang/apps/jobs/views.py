

# Create your views here.

# 王喆--7.2 增加企业访问次数  PUT /enterprise/{id}/visit/
# 王喆--7.5 热门企业  GET /enterprise/search/hotlist/
# 王喆--7.6 收藏公司  Post /enterprise/{id}/collect/
# 王喆--7.11 企业详情 GET /enterprise/{id}/


from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet


from jobs.serializer import CitySerializer, RecruitSerializer, RecruitSerializerSimple
from .models import City, Recruit


class CityViewSet(ModelViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()

    # 梁元朋--获取热门城市列表  city/hotlist/
    @action(methods=["GET"], detail=False, url_path="hotlist")
    def hot_list(self, request):
        cities = self.get_queryset().filter(ishot=1)
        serializer = self.get_serializer(instance=cities, many=True)
        return Response(serializer.data)
# 梁元朋--招聘模块
class RecruitViewSet(ModelViewSet):
    queryset = Recruit.objects.filter(state="1").order_by("-createtime")

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return RecruitSerializer
        else:
            return RecruitSerializerSimple

    # 梁元朋--招聘首页获取最新的4个职位 recruits/search/latest/
    @action(methods=["GET"], detail=False, url_path="search/latest")
    def get_latest_job(self, request):
        jobs = self.get_queryset()[0:4]
        serializer = self.get_serializer(instance=jobs, many=True)
        return Response(serializer.data)

    # 梁元朋--招聘首页获取推荐的4个职位 recruits/search/recommend/
    @action(methods=["GET"], detail=False, url_path="search/recommend")
    def get_recommend_job(self, request):
        jobs = self.get_queryset()[0:4]
        serializer = self.get_serializer(instance=jobs, many=True)
        return Response(serializer.data)


    # 梁元朋--根据城市名称和关键字搜索职位 recruits/search/city/keyword/
    @action(methods=["POST"], detail=False, url_path="search/city/keyword")
    def search_job(self, request):
        cityname = request.data.get('name')
        keyword = request.data.get('keyword')
        jobs = self.get_queryset()
        ret_jobs = []
        if not cityname and not keyword:
            ret_jobs = jobs
        elif cityname and not keyword:
            for job in jobs:
                if job.city == cityname:
                    ret_jobs.append(job)
        elif not cityname and keyword:
            for job in jobs:
                if job.jobname.lower().find(keyword.lower()) != -1:
                    ret_jobs.append(job)
        else:
            for job in jobs:
                if job.city == cityname and job.jobname.lower().find(keyword.lower()) != -1:
                    ret_jobs.append(job)

        serializer = self.get_serializer(instance=ret_jobs, many=True)
        return Response(serializer.data)

    # 梁元朋--更新招聘信息访问量 recruits/{pk}/visit/
    @action(methods=['PUT'], detail=True)
    def visit(self, request, pk):
        recruit = self.get_object()
        recruit.visits += 1
        recruit.save()
        return Response({'success': True, 'message': '更新成功'})

    # 梁元朋--收藏招聘信息 recruits/{pk}/collect/
    @action(methods=['POST'], detail=True)
    def collect(self, request, pk):
        try:
            user = request.user
        except Exception:
            user = None

        if user is not None and user.is_authenticated:
            enterprise = self.get_object()
            enterprise.users.add(user)
            enterprise.save()
            return Response({'success': True, 'message': '收藏成功'})
        else:
            return Response({'success': False, 'message': '未登录'}, status=400)

    # 梁元朋--取消收藏招聘信息 recruits/{pk}/collect/
    @action(methods=['POST'], detail=True)
    def cancelcollect(self, request, pk):
        try:
            user = request.user
        except Exception:
            user = None

        if user is not None and user.is_authenticated:
            enterprise = self.get_object()
            enterprise.users.remove(user)
            enterprise.save()
            return Response({'success': True, 'message': '取消收藏成功'})
        else:
            return Response({'success': False, 'message': '未登录'}, status=400)

from jobs.models import Enterprise
from jobs.serializer import EnterpriseSerializer, EnterpriseSerializerSimple


class EnterpriseViewSet(ModelViewSet):
    queryset = Enterprise.objects.all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return EnterpriseSerializer
        else:
            return EnterpriseSerializerSimple

    # 王喆--7.5 热门企业  GET /enterprise/search/hotlist/
    # @action(methods=["GET"], detail=False, url_path="search/hotlist")
    def get_hot_enterprise(self, request, *args, **kwargs):
        hot_enterprises = self.get_queryset().order_by("-visits")[0:4]
        serializer = self.get_serializer(instance=hot_enterprises, many=True)
        return Response(serializer.data)

    # 王喆--7.2 增加企业访问次数  PUT /enterprise/{id}/visit/
    @action(methods=['put'], detail=True)
    def visit(self, request, pk):
        enterprise = self.get_object()
        enterprise.visits += 1
        enterprise.save()
        return Response({'success':True,'message':'更新成功'})

    # 王喆--7.6.1 收藏公司  Post /enterprise/{id}/collect/
    @action(methods=['post'], detail=True)
    def collect(self, request, pk):
        try:
            user = request.user
        except Exception:
            user = None
        if user is not None and user.is_authenticated:
            recruit = self.get_object()
            recruit.users.add(user)
            recruit.save()
            return Response({'success':True,'message':'收藏成功'})
        else:
            return Response({'success':False,'message':'未登录'}, status=400)



    # 王喆--7.6.2 取消收藏企业 Post enterprise/{id}/cancelcollect/
    @action(methods=['post'], detail=True)

    def cancelcollect(self, request, pk):
        try:
            user = request.user
        except Exception:
            user = None


        if user is not None and user.is_authenticated:
            recruit = self.get_object()
            recruit.users.remove(user)
            recruit.save()
            return Response({'success': True, 'message': '取消收藏成功'})
        else:
            return Response({'success': False, 'message': '未登录'}, status=400)


